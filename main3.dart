import 'package:flutter/material.dart';
import 'package:module5_assignment/main2.dart';
import 'package:module5_assignment/firebase.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class dashboard extends StatelessWidget {
  const dashboard({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Dashboard", textAlign: TextAlign.center),
        ),
        body: Column(
          children: [
            SizedBox(
              height: 50,
              width: 50,
              child: Stack(),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: ElevatedButton(
                      child: const Text(
                        'Form Screen',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                          fixedSize: Size(200, 50),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50))),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  const Formsheet(title: 'Form Page')),
                        );
                      },
                    )),
              ],
            ),
            SizedBox(
              height: 50,
              width: 50,
              child: Stack(),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(
                      child: const Text(
                        'Feature Screen 1',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                          fixedSize: Size(200, 50),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50))),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const FeatureScreen1()),
                        );
                      }),
                ),
              ],
            ),
            SizedBox(
              height: 50,
              width: 50,
              child: Stack(),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(
                      child: const Text(
                        'Feature Screen 2',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                          fixedSize: Size(200, 50),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50))),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const featurescreen2()),
                        );
                      }),
                ),
              ],
            ),
            SizedBox(
              height: 50,
              width: 50,
              child: Stack(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: ElevatedButton(
                      child: const Text(
                        'Edit Profile',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                          fixedSize: Size(200, 50),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50))),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ProfileEdit()),
                        );
                      }),
                ),
              ],
            )
          ],
        ),
        floatingActionButton: SpeedDial(
            animatedIcon: AnimatedIcons.menu_close,
            backgroundColor: Colors.green,
            child: const Icon(Icons.add_box_outlined),
            foregroundColor: Colors.white,
            overlayColor: Colors.green,
            overlayOpacity: 0.4,
            children: [
              SpeedDialChild(
                child: Icon(Icons.pages, color: Colors.green),
                backgroundColor: Colors.white,
                label: "Log Out",
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const login()),
                  );
                },
                labelStyle: TextStyle(color: Colors.green),
              ),
            ]));
  }
}

class FeatureScreen1 extends StatelessWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Feature Screen 1", textAlign: TextAlign.center),
      ),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        backgroundColor: Colors.green,
        child: const Icon(Icons.add_box_outlined),
        foregroundColor: Colors.white,
        overlayColor: Colors.green,
        overlayOpacity: 0.4,
        children: [
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Log Out",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const login()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Dashboard Page",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const dashboard()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Feature Screen 2",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const featurescreen2()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Edit Profile Page",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const ProfileEdit()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
        ],
      ),
    );
  }
}

class featurescreen2 extends StatelessWidget {
  const featurescreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Feature Screen 2", textAlign: TextAlign.center),
      ),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        backgroundColor: Colors.green,
        child: const Icon(Icons.add_box_outlined),
        foregroundColor: Colors.white,
        overlayColor: Colors.green,
        overlayOpacity: 0.4,
        children: [
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Log Out",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const login()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Dashboard Page",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const dashboard()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Feature Screen 1",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const FeatureScreen1()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Edit Profile Page",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const ProfileEdit()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
        ],
      ),
    );
  }
}

class ProfileEdit extends StatelessWidget {
  const ProfileEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile Editing Page", textAlign: TextAlign.center),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 115,
            width: 115,
            child:
                Stack(fit: StackFit.expand, clipBehavior: Clip.none, children: [
              CircleAvatar(
                backgroundColor: Colors.white,
                child: Icon(Icons.camera_alt),
              ),
            ]),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: TextField(
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Edit Profile Name',
              ),
            ),
          ),
          Text(
            "Once profile name edited you will return to the dashboard with the changes made",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                child: const Text('Accept Profile Changes'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const dashboard()),
                  );
                },
              )),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: TextField(
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Change Usename',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: TextField(
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Change Password',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: TextField(
              obscureText: true,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Confirm Password',
              ),
            ),
          ),
          Text(
            "Once account changes have been implemented you will need to login again with new credentals",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                child: const Text('Accept Account Changes'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const login()),
                  );
                },
              )),
        ],
      ),
      floatingActionButton: SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        backgroundColor: Colors.green,
        child: const Icon(Icons.add_box_outlined),
        foregroundColor: Colors.white,
        overlayColor: Colors.green,
        overlayOpacity: 0.4,
        children: [
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Log Out",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const login()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Dashboard Page",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const dashboard()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Feature Screen 1",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const FeatureScreen1()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
          SpeedDialChild(
            child: Icon(Icons.pages, color: Colors.green),
            backgroundColor: Colors.white,
            label: "Feature Screen 2",
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const featurescreen2()),
              );
            },
            labelStyle: TextStyle(color: Colors.green),
          ),
        ],
      ),
    );
  }
}
