import 'package:flutter/material.dart';
import 'package:module5_assignment/main2.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized;
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyBvaMLSp0cHQET9sPFI7JZ-boRNDKZK3AY",
          authDomain: "week6app-ed026.firebaseapp.com",
          projectId: "week6app-ed026",
          storageBucket: "week6app-ed026.appspot.com",
          messagingSenderId: "995521049574",
          appId: "1:995521049574:web:100a1320973f790e785c27"));

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: splash(),
        theme: ThemeData(
            primarySwatch: Colors.green,
            accentColor: Colors.green,
            scaffoldBackgroundColor: Color.fromARGB(255, 141, 207, 143),
            textTheme: TextTheme(
              bodyText2: TextStyle(color: Colors.white),
            ),
            elevatedButtonTheme: ElevatedButtonThemeData(
              style: ElevatedButton.styleFrom(
                  primary: Colors.green, onPrimary: Colors.white),
            ),
            textButtonTheme: TextButtonThemeData(
                style: TextButton.styleFrom(primary: Colors.white))));
  }
}
