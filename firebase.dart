import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:module5_assignment/main3.dart';

class Formsheet extends StatefulWidget {
  const Formsheet({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<Formsheet> createState() => _FormsheetState();
}

class _FormsheetState extends State<Formsheet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[AddSession()]),
          ),
        ));
  }
}

class AddSession extends StatefulWidget {
  const AddSession({Key? key}) : super(key: key);

  @override
  State<AddSession> createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {
  TextEditingController subjectController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController propertyController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();
    TextEditingController propertyController = TextEditingController();

    Future _addSessions() {
      final subject = subjectController.text;
      final location = locationController.text;
      final property = propertyController.text;

      final ref = FirebaseFirestore.instance.collection("sessions").doc();

      return ref
          .set({
            "Subject_Name": subject,
            "location": location,
            "property": property,
            "doc_id": ref.id
          })
          .then((value) => {
                subjectController.text = "",
                locationController.text = "",
                propertyController.text = ""
              })
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: subjectController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText: "Enter chemical name",
                  ),
                )),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: locationController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText: "Enter company the chemical was produced",
                  ),
                )),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                  controller: propertyController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20)),
                    hintText:
                        "Enter the property state of the chemical eg. Solid or Liquid",
                  ),
                )),
            ElevatedButton(
                onPressed: () {
                  _addSessions();
                },
                child: Text("Add Chemical")),
            SizedBox(
              height: 10,
              width: 10,
              child: Stack(),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const dashboard()),
                );
              },
              child: Text("Done"),
            )
          ],
        ),
        SessionList()
      ],
    );
  }
}

class SessionList extends StatefulWidget {
  const SessionList({Key? key}) : super(key: key);

  @override
  State<SessionList> createState() => _SessionListState();
}

class _SessionListState extends State<SessionList> {
  final Stream<QuerySnapshot> _mySessions =
      FirebaseFirestore.instance.collection("sessions").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _subjectFieldCtrlr = TextEditingController();
    TextEditingController _locationFieldCtrlr = TextEditingController();
    TextEditingController _propertyFieldCtrlr = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("sessions")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("sessions");
      _subjectFieldCtrlr.text = data["Subject_Name"];
      _locationFieldCtrlr.text = data["location"];
      _propertyFieldCtrlr.text = data["property"];

      showDialog(
        context: context,
        builder: (_) => AlertDialog(
            title: Center(child: Text("Edit")),
            backgroundColor: Color.fromARGB(255, 141, 207, 143),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextField(
                  controller: _subjectFieldCtrlr,
                ),
                TextField(
                  controller: _locationFieldCtrlr,
                ),
                TextField(
                  controller: _propertyFieldCtrlr,
                ),
                TextButton(
                    onPressed: () {
                      collection.doc(data["doc_id"]).update({
                        "Subject_Name": _subjectFieldCtrlr.text,
                        "location": _locationFieldCtrlr.text,
                        "property": _propertyFieldCtrlr.text
                      });
                      Navigator.pop(context);
                    },
                    child: Text("Update"))
              ],
            )),
      );
    }

    return StreamBuilder(
      stream: _mySessions,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(children: [
            SingleChildScrollView(
                child: SizedBox(
                    height: (MediaQuery.of(context).size.height),
                    width: MediaQuery.of(context).size.width,
                    child: ListView(
                      children: snapshot.data!.docs
                          .map((DocumentSnapshot documentSnapshot) {
                        Map<String, dynamic> data =
                            documentSnapshot.data()! as Map<String, dynamic>;
                        return Column(
                          children: [
                            Card(
                              child: Column(
                                children: [
                                  ListTile(
                                    tileColor:
                                        Color.fromARGB(255, 141, 207, 143),
                                    title: Text(data['Subject_Name']),
                                    subtitle: Text(
                                        "${data["location"]}\n${data["property"]}"),
                                    isThreeLine: true,
                                  ),
                                  ButtonTheme(
                                      child: ButtonBar(
                                    children: [
                                      OutlineButton.icon(
                                          onPressed: () {
                                            _update(data);
                                          },
                                          icon: Icon(Icons.edit),
                                          label: Text("Edit")),
                                      OutlineButton.icon(
                                          onPressed: () {
                                            _delete(data["doc_id"]);
                                          },
                                          icon: Icon(Icons.edit),
                                          label: Text("Delete"))
                                    ],
                                  ))
                                ],
                              ),
                            )
                          ],
                        );
                      }).toList(),
                    )))
          ]);
        } else {
          return (Text("No data"));
        }
      },
    );
  }
}
